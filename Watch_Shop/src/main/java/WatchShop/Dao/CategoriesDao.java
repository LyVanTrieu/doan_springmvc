package WatchShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import WatchShop.Entity.Categories;
import WatchShop.Entity.MapperCategories;

@Repository
public class CategoriesDao extends BaseDao{

	
	public List<Categories> getDataCategories(){
		List<Categories> list = new ArrayList<Categories>();
		String sql = "SELECT * FROM categorys";
		list = _jdbcTemplate.query(sql, new MapperCategories());
		return list;
	}
}
