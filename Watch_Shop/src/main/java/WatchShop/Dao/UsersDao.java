package WatchShop.Dao;

import WatchShop.Entity.MapperUsers;
import WatchShop.Entity.Users;
import org.springframework.stereotype.Repository;


@Repository
public class UsersDao extends BaseDao{

    public int AddAccount(Users user){
        StringBuffer  sql = new StringBuffer();
        sql.append("INSERT ");
        sql.append("INTO users ");
        sql.append("( ");
        sql.append("email, ");
        sql.append("password, ");
        sql.append("firstname, ");
        sql.append("lastname, ");
        sql.append("address, ");
        sql.append("role ");
        sql.append(") ");
        sql.append("VALUES ");
        sql.append("( ");
        sql.append(" '"+ user.getEmail() +"', ");
        sql.append(" '"+ user.getPassword() +"', ");
        sql.append(" '"+ user.getFirstName() +"', ");
        sql.append(" '"+ user.getLastName() +"', ");
        sql.append(" '"+ user.getAddress() +"', ");
        sql.append(" '"+ 0 +"' ");
        sql.append(")");

        int insert = _jdbcTemplate.update(sql.toString());
        return insert;
    }

    public Users getAccount(Users user){
        String sql = "SELECT * FROM users WHERE email = '"+user.getEmail()+"'";
        Users result = _jdbcTemplate.queryForObject(sql, new MapperUsers());
        return result;
    }

    public Users getAccountAdmin(Users user){
        String sql = "SELECT * FROM users WHERE  email = '"+user.getEmail()+"' AND role = 1";
        Users result = _jdbcTemplate.queryForObject(sql, new MapperUsers());
        return result;
    }

    public Boolean getEmail(Users user) {
    	try {
    		String sql = "SELECT * FROM users WHERE email = '"+user.getEmail()+"'";
            Users result = _jdbcTemplate.queryForObject(sql, new MapperUsers());
            if(result.getEmail().equals(user.getEmail())) {
            	return true;
            }
            return false;
		} catch (Exception e) {
			return false;
		}
    }

}
