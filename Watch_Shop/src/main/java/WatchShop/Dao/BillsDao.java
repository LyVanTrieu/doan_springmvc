package WatchShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import WatchShop.Entity.BillDetail;
import WatchShop.Entity.Bills;
import WatchShop.Entity.MapperBills;
import WatchShop.Entity.MapperBillsDetail;
import WatchShop.Entity.MapperSlides;
import WatchShop.Entity.Slides;

@Repository
public class BillsDao extends BaseDao {

	public int AddBills(Bills bills) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO bills");
		sql.append("( ");
		sql.append("email, ");
		sql.append("phone, ");
		sql.append("display_name, ");
		sql.append("address, ");
		sql.append("note ");
		sql.append(") ");
		sql.append("VALUES ");
		sql.append("( ");
		sql.append(" '" + bills.getEmail() + "', ");
		sql.append(" '" + bills.getPhone() + "', ");
		sql.append(" '" + bills.getDisplay_name() + "', ");
		sql.append(" '" + bills.getAddress() + "', ");
		sql.append(" '" + bills.getNote() + "' ");
		sql.append(")");// ;

		int insert = _jdbcTemplate.update(sql.toString());
		return insert;
	}

	public long GetIDLastBills() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT MAX(id) FROM bills");// ;
		long id = _jdbcTemplate.queryForObject(sql.toString(), new Object[] {}, Long.class);
		return id;
	}
	
	public int AddBillsDetail(BillDetail billDetail) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO billdetial");
		sql.append("( ");
		sql.append("	id_product, ");
		sql.append("	id_bills, ");
		sql.append("	quanty, ");
		sql.append("	total ");
		sql.append(") ");
		sql.append("VALUES ");
		sql.append("( ");
		sql.append(" '" + billDetail.getId_product() + "', ");
		sql.append(" '" + billDetail.getId_bills() + "', ");
		sql.append(" '" + billDetail.getQuanty() + "', ");
		sql.append(" '" + billDetail.getTotal() + "' ");
		sql.append(")");// ;

		int insert = _jdbcTemplate.update(sql.toString());
		return insert;
	}
	
//	============== admmin================
	public List<Bills> getBillsData(){
		List<Bills> list = new ArrayList<Bills>();
		String sql = "SELECT * FROM bills";
		list = _jdbcTemplate.query(sql, new MapperBills());
		return list;
	}
	
	public long getTotalRevenue() {
		String sql = "SELECT SUM(total) FROM billdetial";
		long sum = _jdbcTemplate.queryForObject(sql, new Object[] {}, Long.class);
		return sum;
	}
	
	public long getTotalBill() {
		String sql = "SELECT COUNT(id) FROM bills";
		long count = _jdbcTemplate.queryForObject(sql, new Object[] {}, Long.class);
		return count;
	}
	
	public long getTotalProduct() {
		String sql = "SELECT COUNT(id) FROM products";
		long count = _jdbcTemplate.queryForObject(sql, new Object[] {}, Long.class);
		return count;
	}
	
	public long getTotalAccount() {
		String sql = "SELECT COUNT(id) FROM users";
		long count = _jdbcTemplate.queryForObject(sql, new Object[] {}, Long.class);
		return count;
	}
}
