package WatchShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import WatchShop.Entity.MapperMenu;
import WatchShop.Entity.Menus;

@Repository
public class MenuDao extends BaseDao{

	
	public List<Menus> getDataMenus(){
		List<Menus> list = new ArrayList<Menus>();
		String sql = "SELECT * FROM menus";
		list = _jdbcTemplate.query(sql, new MapperMenu());
		return list;
	}
}
