package WatchShop.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import WatchShop.Dto.ProductDto;
import WatchShop.Dto.ProductDtoMapper;

@Repository
public class ProductDao extends BaseDao {
	
	private final boolean YES = true;
	private final boolean NO = false;
	
	private StringBuffer SqlString() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("p.id as id_product ");
		sql.append(", p.id_category ");
		sql.append(", p.name ");
		sql.append(", p.price ");
		sql.append(", p.sale ");
		sql.append(", p.title ");
		sql.append(", p.highlight ");
		sql.append(", p.new_product ");
		sql.append(", p.detail ");
		sql.append(", c.id as id_color ");
		sql.append(", c.name as name_color ");
		sql.append(", c.code as code_color ");
		sql.append(", c.img ");
		sql.append(", p.created_at ");
		sql.append(", p.updated_at ");
		sql.append("FROM ");
		sql.append("products AS p ");
		sql.append("INNER JOIN ");
		sql.append("colors AS c ");
		sql.append("ON p.id = c.id_product ");
		return sql;
	}

	private String SqlProducts(boolean newProduct, boolean highLight) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		if (highLight) {
			sql.append("AND p.highlight = true ");
		}
		if (newProduct) {
			sql.append("AND p.new_product = true ");
		}
		sql.append("GROUP BY p.id, c.id_product ");
		sql.append("ORDER BY RAND() ");
		if (highLight) {
			sql.append("LIMIT 9 ");
		}
		if (newProduct) {
			sql.append("LIMIT 12 ");
		}
		return sql.toString();
	}
	
	private StringBuffer  SqlProductsByID(int id) { //String
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		sql.append("AND id_category = "+id+" ");
		return sql; //.toString
	}
	
	private String SqlProductsPaginate(int id, int start, int totalPage) {
		StringBuffer sql = SqlProductsByID(id);
		sql.append("LIMIT " + start + ", "+ totalPage);
		return sql.toString();
	}
	
	public List<ProductDto> GetAllProductsByID(int id) {
		String sql = SqlProductsByID(id).toString();//.toString()
		List<ProductDto> listProducts = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return listProducts;
	}
	
	public List<ProductDto> GetDataProductsPaginate(int id, int start, int totalPage) {
		StringBuffer sqlGetDataByID = SqlProductsByID(id);
		List<ProductDto> listProductsByID = _jdbcTemplate.query(sqlGetDataByID.toString(), new ProductDtoMapper());
		List<ProductDto> listProducts = new ArrayList<ProductDto>();
		if(listProductsByID.size() > 0) {
			String sql = SqlProductsPaginate(id, start, totalPage);
			listProducts = _jdbcTemplate.query(sql, new ProductDtoMapper());
		}
		return listProducts;
	}
	
	private String SqlProductByID(long id) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		sql.append("AND p.id = " + id + " ");
		sql.append("LIMIT 1 ");
		return sql.toString();
	}
	
	public List<ProductDto> GetProductByID(long id) {
		String sql = SqlProductByID(id);
		List<ProductDto> listProduct = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return listProduct;
	}
	

	public List<ProductDto> GetDataProducts() {
		String sql = SqlProducts(YES, NO);
		List<ProductDto> listProducts = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return listProducts;
	}
	
	public List<ProductDto> GetDataNewProducts() {
		String sql = SqlProducts(NO, YES);
		List<ProductDto> listProducts = _jdbcTemplate.query(sql, new ProductDtoMapper());
		return listProducts;
	}
	
	public ProductDto FindProductByID(long id) {
		String sql = SqlProductByID(id);
		ProductDto product = _jdbcTemplate.queryForObject(sql, new ProductDtoMapper());
		return product;
	}
}
