package WatchShop.UserController;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class HomeController extends BaseController {


	@RequestMapping(value = { "/", "/trang-chu" })
	public ModelAndView Index() {
		_mvShare.setViewName("user/index");
		_mvShare.addObject("slides", _homeServiceImpl.GetDataSlide());
		_mvShare.addObject("categories", _homeServiceImpl.GetDataCategories());
		_mvShare.addObject("products", _homeServiceImpl.GetDataProductDto());
		_mvShare.addObject("newProducts", _homeServiceImpl.GetDataNewProductDto());
		return _mvShare;
	}

//	@RequestMapping(value = { "/product" })
//	public ModelAndView Product() {
//		_mvShare.setViewName("user/products/product");
//		_mvShare.addObject("slides", _homeServiceImpl.GetDataSlide());
//		_mvShare.addObject("categories", _homeServiceImpl.GetDataCategories());
//		_mvShare.addObject("products", _homeServiceImpl.GetDataProductDto());
//		_mvShare.addObject("newProducts", _homeServiceImpl.GetDataNewProductDto());
//		return _mvShare;
//	}
//	

}
