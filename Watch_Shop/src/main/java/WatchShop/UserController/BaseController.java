package WatchShop.UserController;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import WatchShop.Service.Admin.AdminHomeServiceImpl;
import WatchShop.Service.User.HomeServiceImpl;

@Controller
public class BaseController {
	@Autowired
	HomeServiceImpl _homeServiceImpl;

	public ModelAndView _mvShare = new ModelAndView("user/index");
	
	@PostConstruct         
	public ModelAndView Init() {
		return _mvShare;
	}
}
