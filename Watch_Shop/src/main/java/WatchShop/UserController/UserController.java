package WatchShop.UserController;

import WatchShop.Dao.UsersDao;
import WatchShop.Entity.Users;
import WatchShop.Service.User.AccountServiceImpl;
//import WatchShop.Validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.ErrorData;
import javax.validation.Valid;

@Controller
public class UserController extends BaseController {
    @Autowired
    AccountServiceImpl accountService = new AccountServiceImpl();
    @Autowired
    UsersDao userDao = new UsersDao();

//    private UserValidator userValidator = new UserValidator();

    @RequestMapping(value = "/dang-ky", method = RequestMethod.GET)
        public ModelAndView Register(){
        _mvShare.setViewName("user/register");
        _mvShare.addObject("user",new Users());
        return _mvShare;
    }
    @RequestMapping(value = "/dang-ky", method = RequestMethod.POST)
    public ModelAndView CreateAccount( @Valid @ModelAttribute("user") Users user, Errors error,BindingResult rs){
        int count = 0;
        if(rs.hasErrors()){
            System.out.println(rs);
            _mvShare = new ModelAndView("user/register");				// Phải trả về như vầy nó mới show error ra được 
            return _mvShare;
        }else{
            boolean check = accountService.isEmailExist(user.getEmail(), user);
            if(!check){
                count = accountService.AddAccount(user);
                if(count > 0){
                    _mvShare.addObject("count", count);
                    _mvShare.setViewName("redirect:/dang-nhap");
                }
            }else{
                _mvShare.addObject("signup", "Email đã tồn tại");
                _mvShare.addObject("count", count);
                _mvShare.setViewName("user/register");
                return _mvShare;
            }
        }
        return _mvShare;
    }

    @RequestMapping(value = "/dang-nhap", method = RequestMethod.GET)
    public ModelAndView DangNhap(){
        _mvShare = new ModelAndView("user/login");
        _mvShare.addObject("user", new Users());
        return _mvShare;
    }

    @RequestMapping(value = "/dang-nhap", method = RequestMethod.POST)
    public ModelAndView LogIn(HttpSession session, @ModelAttribute("user") Users user, BindingResult rs){
        boolean check = accountService.checkAccount(user);
        if(check){
            user = userDao.getAccount(user);
            _mvShare.setViewName("redirect:/trang-chu");
            session.setAttribute("loginInfo", user);
        }else{
            _mvShare.addObject("status","Sai mật khẩu hoặc tài khoản");
            _mvShare.setViewName("user/login");
        }
        return _mvShare;
    }
}
