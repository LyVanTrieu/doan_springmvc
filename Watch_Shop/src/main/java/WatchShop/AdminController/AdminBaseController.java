package WatchShop.AdminController;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import WatchShop.Service.Admin.AdminHomeServiceImpl;

@Controller
public class AdminBaseController {	
	@Autowired
	AdminHomeServiceImpl _adminHomeServiceImpl;
	
	public ModelAndView _mvShareAdmin = new ModelAndView("admin/index");
	
	@PostConstruct         
	public ModelAndView Init() {
		return _mvShareAdmin;
	}
}
