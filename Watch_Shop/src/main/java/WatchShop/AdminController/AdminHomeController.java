package WatchShop.AdminController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class AdminHomeController extends AdminBaseController{
	
	@RequestMapping(value = { "/admin"})
	public ModelAndView Index() {
		_mvShareAdmin.setViewName("admin/index");
		_mvShareAdmin.addObject("allBills", _adminHomeServiceImpl.getBillsData());
		_mvShareAdmin.addObject("revenue", _adminHomeServiceImpl.getTotalRevenue());
		_mvShareAdmin.addObject("billVolume", _adminHomeServiceImpl.getTotalBill());
		_mvShareAdmin.addObject("productVolume", _adminHomeServiceImpl.getTotalProduct());
		_mvShareAdmin.addObject("accountVolume", _adminHomeServiceImpl.getTotalAccount());
		return _mvShareAdmin;
	}
}
