package WatchShop.AdminController;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WatchShop.Dao.UsersDao;
import WatchShop.Entity.Users;
import WatchShop.Service.User.AccountServiceImpl;

@Controller
public class AdminLoginController extends AdminBaseController{
	
	@Autowired
    AccountServiceImpl accountService = new AccountServiceImpl();
    @Autowired
    UsersDao userDao = new UsersDao();
	
	@RequestMapping(value = "/admin-dang-nhap", method = RequestMethod.GET)
	public ModelAndView ShowLoginForm() {
		_mvShareAdmin = new ModelAndView("layouts/login");
		_mvShareAdmin.addObject("admin", new Users());
		return _mvShareAdmin;
	}
	
	@RequestMapping(value = "/admin-dang-nhap", method = RequestMethod.POST)
    public ModelAndView LogIn(HttpSession session, @ModelAttribute("admin") Users user, BindingResult rs){
		boolean check = accountService.checkAdminAccount(user);
        if(check){
            user = userDao.getAccount(user);
            _mvShareAdmin.setViewName("redirect:/admin");
            session.setAttribute("loginInfo", user);
        }else{
            _mvShareAdmin.addObject("status","Sai mật khẩu hoặc tài khoản");
        }
        return _mvShareAdmin;
    }
}
