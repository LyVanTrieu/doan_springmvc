package WatchShop.Service.Admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import WatchShop.Dao.BillsDao;
import WatchShop.Entity.Bills;

@Service
public class AdminHomeServiceImpl implements IAdminHomeService{

	@Autowired
	private BillsDao billsDao;
	@Override
	public List<Bills> getBillsData() {
		
		return billsDao.getBillsData();
	}

	@Override
	public String getTotalRevenue() {
		
		return String.valueOf(billsDao.getTotalRevenue());
	}
	
	@Override
	public long getTotalBill() {
		
		return billsDao.getTotalBill();
	}

	@Override
	public long getTotalProduct() {
		// TODO Auto-generated method stub
		return billsDao.getTotalProduct();
	}

	@Override
	public long getTotalAccount() {
		// TODO Auto-generated method stub
		return billsDao.getTotalAccount();
	}

}
