package WatchShop.Service.Admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import WatchShop.Entity.Bills;

@Service
public interface IAdminHomeService {
	
	@Autowired
	public List<Bills> getBillsData();
	public String getTotalRevenue();
	public long getTotalBill();
	public long getTotalProduct();
	public long getTotalAccount();
}
