package WatchShop.Service.User;

import WatchShop.Dao.UsersDao;
import WatchShop.Entity.Users;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements IAccountService{
    @Autowired
    UsersDao userDao = new UsersDao();

    @Override
    public int AddAccount(Users user) {
        user.setPassword(BCrypt.hashpw(user.getPassword(),BCrypt.gensalt(12)));

        return userDao.AddAccount(user);
    }

    @Override
    public boolean checkAccount(Users user) {
        try {
        	String pass = user.getPassword();   //lay pass word truoc khi cap nhat lai user moi
            user = userDao.getAccount(user);
            if(user != null){
                if(BCrypt.checkpw(pass,user.getPassword())){
                    return true;
                }else{
                    return false;
                }
            }
            return false;
		} catch (Exception e) {
			return false;
		}
    }

	@Override
	public boolean isEmailExist(String email, Users user) {
		Boolean check = userDao.getEmail(user);
        if(check){
           return true;		// lay email ra so sanh vs user.getemail
        }
		return false;
	}

	@Override
	public boolean checkAdminAccount(Users user) {
		try {
        	String pass = user.getPassword();   //lay pass word truoc khi cap nhat lai user moi
            user = userDao.getAccountAdmin(user);
            if(user != null){
                if(BCrypt.checkpw(pass,user.getPassword()) && user.getRole() == 1){
                    return true;
                }else{
                    return false;
                }
            }
            return false;
		} catch (Exception e) {
			return false;
		}
	}
}
