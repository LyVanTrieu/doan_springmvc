package WatchShop.Service.User;

import WatchShop.Entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public interface IAccountService {
    @Autowired
    public int AddAccount(Users user);
    @Autowired
    public boolean checkAccount(Users user);
    @Autowired
    public boolean checkAdminAccount(Users user);
    @Autowired
    public boolean isEmailExist(String email, Users user);
}
