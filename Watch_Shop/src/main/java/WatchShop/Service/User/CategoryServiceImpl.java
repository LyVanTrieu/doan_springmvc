package WatchShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import WatchShop.Dao.ProductDao;
import WatchShop.Dto.ProductDto;

@Service
public class CategoryServiceImpl implements ICategoryService{

	@Autowired
	private ProductDao productsDao;
	
	public List<ProductDto> GetAllProductsByID(int id) {
		return productsDao.GetAllProductsByID(id);
	}
	
	public List<ProductDto> GetDataProductsPaginate(int id, int start, int totalPage) {
		return productsDao.GetDataProductsPaginate(id, start, totalPage);
	}
	
}