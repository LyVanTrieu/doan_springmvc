package WatchShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import WatchShop.Dao.CategoriesDao;
import WatchShop.Dao.MenuDao;
import WatchShop.Dao.ProductDao;
import WatchShop.Dao.SlidesDao;
import WatchShop.Dto.ProductDto;
import WatchShop.Entity.Categories;
import WatchShop.Entity.Menus;
import WatchShop.Entity.Slides;

@Service
public class HomeServiceImpl implements IHomeService {
	@Autowired
	private SlidesDao slidesDao;
	@Autowired
	private CategoriesDao categoriesDao;
	@Autowired
	private MenuDao menuDao;
	@Autowired
	private ProductDao productDao;
	
	@Override
	public List<Slides> GetDataSlide() {
		
		return slidesDao.GetDataSlide();
	}
	public List<Categories> GetDataCategories() {
		
		return categoriesDao.getDataCategories();
	}
	public List<Menus> GetDataMenus() {
		
		return menuDao.getDataMenus();
	}
	@Override
	public List<ProductDto> GetDataProductDto() {
		List<ProductDto> listProducts = productDao.GetDataProducts();
		listProducts.get(0).getId_color();
		return listProducts;
	}
	
	@Override
	public List<ProductDto> GetDataNewProductDto() {
		List<ProductDto> listProducts = productDao.GetDataNewProducts();
		listProducts.get(0).getId_color();
		return listProducts;
	}

}
