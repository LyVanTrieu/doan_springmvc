package WatchShop.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import WatchShop.Dto.ProductDto;
import WatchShop.Entity.Categories;
import WatchShop.Entity.Menus;
import WatchShop.Entity.Slides;

@Service
public interface IHomeService {
	@Autowired
	public List<Slides> GetDataSlide();
	public List<Categories> GetDataCategories();
	public List<Menus> GetDataMenus();
	public List<ProductDto> GetDataProductDto();
	public List<ProductDto> GetDataNewProductDto();
}
