package WatchShop.Service.User;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import WatchShop.Dto.CartDto;
import WatchShop.Entity.Bills;

@Service
public interface IBillsService {
	public int AddBills(Bills bill);
	public void AddBillsDetail(HashMap<Long, CartDto> carts);
}
