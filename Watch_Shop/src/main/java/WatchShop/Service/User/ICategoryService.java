package WatchShop.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import WatchShop.Dto.ProductDto;

@Service
public interface ICategoryService {
	
	public List<ProductDto> GetAllProductsByID(int id);
	
}
