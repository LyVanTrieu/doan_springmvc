package WatchShop.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperBillsDetail implements RowMapper<BillDetail>{

	@Override
	public BillDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		BillDetail bd = new BillDetail();
		bd.setId(rs.getInt("id"));
		bd.setId_product(rs.getLong("id_product"));
		bd.setId_bills(rs.getLong("id_bills"));
		bd.setQuanty(rs.getInt("quanty"));
		bd.setTotal(rs.getDouble("total"));
		return bd;
	}
	
}
