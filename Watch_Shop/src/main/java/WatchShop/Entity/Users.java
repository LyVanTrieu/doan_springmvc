package WatchShop.Entity;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Users {
    private long id;
    @Valid
    @NotEmpty(message = "Email không được để trống")
    private String email;
    @Valid
    @NotEmpty(message = "password không được bỏ trống")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z]).{6,50}$", message = "password phải chứ ký tự hoa, 6-50 ký tự")
    private String password;
    @Valid
    @NotEmpty(message = "họ không được bỏ trống")
    private String firstName;
    @Valid
    @NotEmpty(message = "tên không được bỏ trống")
    private String lastName;
    private String address;
    private int role;


	public Users() {
    }

    public Users(long id, String email, String password, String firstName, String lastName, String address, int role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}
    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
