<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="<c:url value="assets/user/watch1.png"/>">

    <link rel="stylesheet" href="<c:url value="/assets/user/css/icomoon/style.css"/>">

    <link rel="stylesheet" href="<c:url value="/assets/user/css/owl.carousel.min.css"/>">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<c:url value="/assets/user/css/bootstrap.min.css"/>">

    <!-- Style -->
    <link rel="stylesheet" href="<c:url value="/assets/user/css/style.css"/>">

    <title>Đăng ký</title>
    <style type="text/css">
    	.error{
    		color: red;
    		font-size: 13px;
    	}
    	.half, .half .container > .row {
		    height: 100vh;
		    min-height: 1000px;
		}
    </style>
</head>
<body>

<div class="site-wrap">
    <div class="d-md-flex half">
        <div class="contents">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-12">
                        <div class="form-block mx-auto">
                            <div class="text-center mb-5">
                                <h3 class="text-uppercase">Đăng ký tài khoản</h3>
                            </div>
<%--                            ben thang usercontroller addobject thang nao thi ben day bo thang do vao--%>
                            <form:form action="dang-ky" method="POST" modelAttribute="user" cssClass="fmrt">
                                <div class="form-group last mb-3">
                                    <label for="firstName">Họ </label> <br/> <form:errors path="firstName" cssClass="error"/>
                                    <form:input path="firstName" class="form-control" placeholder="Nguyễn" type="text" name="firstName"/>
                                   
                                </div>
                                <div class="form-group last mb-3">
                                    <label for="lastName">Tên</label> <br/> <form:errors path="lastName" cssClass="error"/>
                                    <form:input path="lastName" class="form-control" placeholder="Văn A" type="text" name="lastName" />
                                </div>
                                <div class="form-group first">
                                    <label for="email">Email</label> <br/> <form:errors path="email" cssClass="error"/>
                                    <form:input path="email" class="form-control" placeholder="your-email@gmail.com" type="email" name="email" />
                                </div>
                                <div class="form-group last mb-3">
                                    <label for="password">Mật khẩu</label> <br/>
                                    <form:input path="password" class="form-control" placeholder="mật khẩu" type="password" name="password" />
                                	<form:errors path="password" cssClass="error"/>
                               	</div>
                                <div class="form-group last mb-3">
                                    <label for="address">Địa chỉ</label>
                                    <form:input path="address" class="form-control" type="text" placeholder="địa chỉ chi tiết" id="address" name="address" />
                                </div>
                                <div class="d-sm-flex mb-5 align-items-center">
                                    <span class="ml-auto"> <a href="<c:url value="/dang-nhap"/>" class="forgot-pass">Về trang đăng nhập</a></span>
                                </div>
                                <c:choose>
                                    <c:when test="${count > 0}">
                                        <div class="alert alert-light" role="alert" style="color: green">
                                            <c:out value="${signup}"/>
                                        </div>
                                    </c:when>
                                    <c:when test="${count == 0}">
                                        <div class="alert alert-light" role="alert" style="color: red">
                                            <c:out value="${signup}"/>
                                        </div>
                                    </c:when>
                                </c:choose>
                                <input type="submit" id="submit" value="Đăng ký" class="btn btn-block py-2 btn-primary">

                                <!-- <span class="text-center my-3 d-block">or</span>


                                <div class="">
                                <a href="#" class="btn btn-block py-2 btn-facebook">
                                  <span class="icon-facebook mr-3"></span> Login with facebook
                                </a>
                                <a href="#" class="btn btn-block py-2 btn-google"><span class="icon-google mr-3"></span> Login with Google</a>
                                </div> -->
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<c:url value="/assets/user/js/login/jquery-3.3.1.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/popper.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/bootstrap.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/main.js"/>"></script>
</body>
</html>