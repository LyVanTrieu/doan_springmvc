<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>



<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>


<link rel="shortcut icon"
	href="<c:url value="/assets/user/watch1.png"/>">
<style>
</style>
</head>


<body>
	<div class="bg-light py-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mb-0">
					<a href='<c:url value="/"/>'>Trang chủ</a> <span class="mx-2 mb-0">/</span>
					<strong class="text-black">${product.name}</strong>
				</div>
			</div>
		</div>
	</div>

	<div class="site-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<img src="<c:url value="${product.img }"/>" alt="Image"
						class="img-fluid" data-pagespeed-url-hash="1414887416"
						onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
				</div>
				<form class="col-md-6" method="get" action='<c:url value="/AddCart/${product.id_product }"/>'>
					<div  >
					<h2 class="text-black">${product.name }</h2>
					<p>${product.details }</p>


					<p>

						<strong class="text-primary h4">
							<p class="text-primary font-weight-bold">
								<fmt:formatNumber type="number" groupingUsed="true"
									value="${product.price}" />
								₫
							</p>
						</strong>
					</p>

					<div class="mb-5">
						<label class="bg-light py-1" style="color: blue;"><span>Số Lượng: 
								&nbsp;</span>
							<div class="buttons_added" style="float: right;">

								<input  id="quanty-cart-${ cart.key }"  aria-label="quantity" class="input-qty" max="10" min="1"
									name="" type="number" value="1" style="color: red;">

							</div> </label>
						<div class="input-group mb-3" style="float: left;">
							<label class="bg-light py-1" style="color: blue;"><span>Màu sắc:
									&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<div class="controls" style="float: right;">
									<select class="span11">
										<option>Red</option>
										<option>Purple</option>
										<option>Pink</option>
										<option>Red</option>
									</select>
								</div> </label>

						</div>
					</div>

					<p>
						<button type="submit" data-id="${ cart.key }" class="buy-now btn btn-sm btn-primary edit-cart"><span class="icon-shopping-cart"></span> Thêm giỏ hàng</button>
					</p>
				</div>
				</form>
				
			</div>
		</div>
	</div>

	<div class="site-section block-3 site-blocks-2 bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 site-section-heading text-center pt-4">
					<h2>Sản Phẩm Liên Quan</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="nonloop-block-3 owl-carousel">
						<c:if test="${productByIDCategory.size() > 0 }">
							<div class="item">

								<c:forEach var="item" items="${productByIDCategory }"
									varStatus="loop">
									<div class="block-4 text-center">
										<figure class="block-4-image">
											<a
												href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">
												<img src="<c:url value="${item.img }"/>"
												alt="Image placeholder" class="img-fluid"
												data-pagespeed-url-hash="1414887416"
												onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
											</a>

										</figure>
										<div class="block-4-text p-4">
											<h3>
												<a
													href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">${item.name }</a>
											</h3>
											<p class="mb-0">${item.title }</p>
											<p class="text-primary font-weight-bold">
												<fmt:formatNumber type="number" groupingUsed="true"
													value="${item.price}" />
												₫
											</p>
										</div>
									</div>

									<c:if
										test="${ (loop.index + 1) % 1 == 0 || (loop.index + 1) == productByIDCategory.size() }">
							</div>
							<c:if test="${ (loop.index + 1) < productByIDCategory.size()}">
								<div class="item">
							</c:if>
						</c:if>
						</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
<content tag="script">
		<script >
				$(".edit-cart").on("click",function(){
					var id = $(this).data("id");
					var quanty = $('#quanty-cart-'+id).val();
					window.location = "EditCart/"+id+"/"+quanty;
				});
		</script>
</content>



</body>
