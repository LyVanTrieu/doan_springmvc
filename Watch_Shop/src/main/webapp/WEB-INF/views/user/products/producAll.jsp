<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>

<head>
<title>Sản - Phẩm</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="shortcut icon"
	href="<c:url value="/assets/user/watch1.png"/>">

<!-- Bootstrap styles -->
<link href="<c:url value="/assets/user/css1/bootstrap.css" />"
	rel="stylesheet" />
<!-- Customize styles -->
<link href="<c:url value="/assets/user/style1.css" />" rel="stylesheet" />
<!-- font awesome styles -->
<link
	href="<c:url value="/assets/user/font-awesome/css/font-awesome.css" />"
	rel="stylesheet">

<style>
.pagination {
	display: flex;
	justify-content: center;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
	border: 1px solid #ddd;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
	border: 1px solid #4CAF50;
}

.pagination a:hover:not(.active) {
	background-color: #ddd;
}
</style>

</head>

<body>
<div class="site-section site-blocks-2">
		<div class="container">
			<div class="row">
				<c:forEach var="item" items="${categories}">
					<div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0"
						data-aos="fade" data-aos-delay="">
						<a class="block-2-item" href="<c:url value="/san-pham/${item.id }"/>">
							<figure class="image">

								<img src="<c:url value="${item.description }"/>" alt=""
									class="img-fluid" data-pagespeed-url-hash="2182569662"
									onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
							</figure>
							<div class="text" >							
								<h3 style="text-align: center; text-decoration:blink; color:silver;">
									${item.name}
								</h3>
							</div>
						</a>
					</div>
				</c:forEach>

			</div>
		</div>
	</div>
	
	
		<div class="site-section block-3 site-blocks-2 bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 site-section-heading text-center pt-4">
					<h2>Sản Phẩm Mới</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="nonloop-block-3 owl-carousel">
						<c:if test="${newProducts.size() > 0 }">
							<div class="item">

								<c:forEach var="item" items="${newProducts }" varStatus="loop">
									<div class="block-4 text-center">
										<figure class="block-4-image">
										<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">
											<img src="<c:url value="${item.img }"/>"
												alt="Image placeholder" class="img-fluid"
												data-pagespeed-url-hash="1414887416"
												onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
										</figure>
										<div class="block-4-text p-4">
											<h3>
												<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">${item.name }</a>
											</h3>
											<p class="mb-0">${item.title }</p>
											<p class="text-primary font-weight-bold">
												<fmt:formatNumber type="number" groupingUsed="true"
													value="${item.price}" />
												₫
											</p>
										</div>
									</div>

									<c:if
										test="${ (loop.index + 1) % 1 == 0 || (loop.index + 1) == products.size() }">
							</div>
							<c:if test="${ (loop.index + 1) < products.size()}">
								<div class="item">
							</c:if>
						</c:if>
						</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
</body>
