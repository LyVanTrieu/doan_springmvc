<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp" %>   
<head>
<title>Giỏ hàng</title>
<link rel="shortcut icon"
	href="<c:url value="/assets/user/watch1.png"/>">

<!-- Bootstrap styles -->
<link href="<c:url value="/assets/user/css1/bootstrap.css" />"
	rel="stylesheet" />
<!-- Customize styles -->
<link href="<c:url value="/assets/user/style1.css" />" rel="stylesheet" />
<!-- font awesome styles -->
<link
	href="<c:url value="/assets/user/font-awesome/css/font-awesome.css" />"
	rel="stylesheet">
	
	    <style type="text/css">
        html, body {
          height: 100%;
        }

        .purple-square-container {
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: center;
        }

        .purple-square {
          background-color: white;
          width: 950px;
          height: 800px;
        }
    </style>
</head>
<body>


 <div class="purple-square-container">
        <div class="purple-square">
        <div class="row" >
	<div class="span12">
    <ul class="breadcrumb">
		<li><a href='<c:url value="/"></c:url>'>Trang chủ</a> <span class="divider">/</span></li>
		<li class="active">Giỏ hàng</li>
    </ul>
	<div class="well well-small">
		<h1>Giỏ hàng <small class="pull-right"></small></h1>
		<small class="pull-right">Tổng tiền: <fmt:formatNumber type="number" groupingUsed="true" value="${TotalPriceCart}" />₫</small>
	<hr class="soften"/>	

	<table class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th>Hình ảnh</th>
                  <th>Mô tả</th>
                  <th>Màu sắc</th>
                  <th>Giá bán</th>
                  <th>Số lượng </th>
                  <th>Chỉnh sửa </th>
                  <th>Xóa </th>
                  <th>Tổng tiền</th>
				</tr>
              </thead>
              <tbody>
              
				<c:forEach var="item" items="${ Cart }">
					<tr>
	                  <td><img width="100" src="<c:url value="${ item.value.product.img }"/>" alt=""></td>
	                  <td>${ item.value.product.title }</td>
	                  <td><span class="shopBtn" style="background-color:${ item.value.product.code_color } ;"><span class="icon-ok"></span></span> </td>
	                  <td><fmt:formatNumber type="number" groupingUsed="true" value="${ item.value.product.price }"/> ₫</td>
	                  <td>
						<input type="number" min="0" max="1000" class="span1" style="max-width:100px" placeholder="1" id="quanty-cart-${ item.key }" size="15" type="text" value="${ item.value.quanty }">
					  </td>
	                  <td>
	                  	<button  data-id="${ item.key }" class="btn btn-mini btn-danger edit-cart" type="button">
	                  		<span class="icon-edit"></span>
	                  	</button>
	                  </td>
	                  <td>
	                  	<a href="<c:url value="/DeleteCart/${ item.key }"/>" class="btn btn-mini btn-danger" type="button">
	                  		<span class="icon-remove"></span>
	                  	</a>
	                  </td>
	                  <td><fmt:formatNumber type="number" groupingUsed="true" value="${ item.value.totalPrice }"/> ₫</td>
	                </tr>
				</c:forEach>
                
				</tbody>
            </table><br/>
			
	<a href='<c:url value="/tat-ca-sp"/>' class="shopBtn btn-large"><span class="icon-arrow-left"></span> Tiếp tục mua sắm </a>
	<a href='<c:url value="checkout"/>' class="shopBtn btn-large pull-right">Thanh toán <span class="icon-arrow-right"></span></a>

</div>
</div>
</div>
</div>
</div>

<content tag="script">
		<script >
				$(".edit-cart").on("click",function(){
					var id = $(this).data("id");
					var quanty = $('#quanty-cart-'+id).val();
					window.location = "EditCart/"+id+"/"+quanty;
				});
		</script>
</content>
		


</body>