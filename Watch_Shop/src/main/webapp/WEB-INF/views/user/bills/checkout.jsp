
<!doctype html>
<html lang="en">
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<head>
<!-- Required meta tags -->
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link
	href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap"
	rel="stylesheet">
<link rel="shortcut icon" href="<c:url value="assets/user/watch1.png"/>">

<link rel="stylesheet"
	href="<c:url value="/assets/user/css/icomoon/style.css"/>">

<link rel="stylesheet"
	href="<c:url value="/assets/user/css/owl.carousel.min.css"/>">
<!-- Bootstrap styles -->
<link href="<c:url value="/assets/user/css1/bootstrap.css" />"
	rel="stylesheet" />
<!-- Customize styles -->
<link href="<c:url value="/assets/user/style1.css" />" rel="stylesheet" />

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="<c:url value="/assets/user/css/bootstrap.min.css"/>">

<!-- Style -->
<link rel="stylesheet"
	href="<c:url value="/assets/user/css/style.css"/>">
<style type="text/css">
html, body {
	height: 100%;
}

.purple-square-container {
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
}

.purple-square {
	background-color: white;
	width: 950px;
	height: 850px;
}
</style>
<title>Thanh toán</title>
</head>
<body>
	<div class="purple-square-container">
		<div class="purple-square">

			<div class="site-wrap">


				<div class="d-md-flex half">
					<div class="contents">

						<div class="container">
							<div class="row align-items-center justify-content-center">
								<div class="col-md-12">
									<div class="form-block mx-auto">
										<div class="text-center mb-5">
											<h3 class="text-uppercase"><strong>Thanh toán</strong></h3>
										</div>
				
										<form:form action="checkout" method="POST" modelAttribute="bills" class="form-horizontal">
											<strong><p style="color: black; font-weight: 200">Khách hàng: ${loginInfo.lastName } </p></strong>
											<div class="control-group">
												<label class="control-label">Họ tên<sup>*</sup></label>
												<div class="controls">
													<form:input type="text" placeholder="Nhập họ và tên" path="display_name"/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Email <sup>*</sup></label>
												<div class="controls">
													<form:input type="email" class="span3" placeholder=" Nhập email" path="email"/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Số điện thoại <sup>*</sup></label>
												<div class="controls">
													<form:input type="text" class="span3" placeholder=" Nhập số điện thoại" path="phone"/>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Địa chỉ<sup>*</sup></label>
												<div class="controls" >
													<form:textarea path="address" row="5" cols="30" />
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Ghi chú<sup>*</sup></label>
												<div class="controls">
													<form:textarea path="note" row="5" cols="30" />
												</div>
											</div>
											<div class="control-group">
												<div class="controls">
													<input type="submit" name="submitAccount" value="Đặt hàng"
														class="shopBtn exclusive">
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>



	<script
		src="<c:url value="/assets/user/js/login/jquery-3.3.1.min.js"/>"></script>
	<script src="<c:url value="/assets/user/js/login/popper.min.js"/>"></script>
	<script src="<c:url value="/assets/user/js/login/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/assets/user/js/login/main.js"/>"></script>
</body>
</html>
