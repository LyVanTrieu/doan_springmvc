<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!Doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="<c:url value="assets/user/watch1.png"/>">

  <link rel="stylesheet" href="<c:url value="/assets/user/css/icomoon/style.css"/>">

  <link rel="stylesheet" href="<c:url value="/assets/user/css/owl.carousel.min.css"/>">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<c:url value="/assets/user/css/bootstrap.min.css"/>">

  <!-- Style -->
  <link rel="stylesheet" href="<c:url value="/assets/user/css/style.css"/>">

  <title>Đăng nhập</title>
</head>
<body>

<div class="site-wrap">

  <div class="d-md-flex half">
    <div class="contents">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-12">
            <div class="form-block mx-auto">
              <div class="text-center mb-5">
                <h3 class="text-uppercase">Đăng nhập</h3>
              </div>
              <form:form action="dang-nhap" method="POST" modelAttribute="user">
                <div class="form-group first">
                  <label for="email">Email</label>
                  <form:input class="form-control" placeholder="your-email@gmail.com" type="email" path="email"/>
                </div>
                <form:errors path="email" cssClass="error"/>
                <div class="form-group last mb-3">
                  <label for="password">Mật khẩu</label>
                  <form:input class="form-control" placeholder="mật khẩu" type="password" path="password"/>
                </div>
                <form:errors path="password" cssClass="error"/>
                <div class="d-sm-flex mb-5 align-items-center">
                  <label class="control control--checkbox mb-3 mb-sm-0"><span class="caption">Ghi nhớ tài khoản</span>
                    <input type="checkbox" checked="checked"/>
                    <div class="control__indicator"></div>
                  </label>
                  <span class="ml-auto"><a href="#" class="forgot-pass">Quên mật khẩu</a></span>
                </div>
                <div class="d-sm-flex mb-5 align-items-center">
                  <span class="ml-auto">Chưa có tài khoản?<a href="<c:url value="/dang-ky"/>" class="forgot-pass"> Đăng ký</a></span>
                </div>
                <c:choose>
                  <c:when test="${empty status}">
                    <div class="alert alert-light" role="alert" style="color: green">

                    </div>
                  </c:when>
                  <c:when test="${not empty status}">
                    <div class="alert alert-light" role="alert" style="text-align: center; color: red; ">
                        ${status}
                      <c:set var="status" value="null"/>
                    </div>
                  </c:when>
                </c:choose>
                <input type="submit" id="submit" value="Đăng nhập" class="btn btn-block py-2 btn-primary" data-dimiss="alert">

                <!-- <span class="text-center my-3 d-block">or</span>


                <div class="">
                <a href="#" class="btn btn-block py-2 btn-facebook">
                  <span class="icon-facebook mr-3"></span> Login with facebook
                </a>
                <a href="#" class="btn btn-block py-2 btn-google"><span class="icon-google mr-3"></span> Login with Google</a>
                </div> -->
              </form:form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="<c:url value="/assets/user/js/login/jquery-3.3.1.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/popper.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/bootstrap.min.js"/>"></script>
<script src="<c:url value="/assets/user/js/login/main.js"/>"></script>

<%--<script>--%>
<%--  $(document).ready(function() {--%>
<%--    $(".alert-light").hide();--%>
<%--    $("#submit").click(function showAlert() {--%>
<%--      $(".alert-light").fadeTo(2000, 1000).slideUp(1000, function() {--%>
<%--        $(".alert-light").slideUp(1000);--%>
<%--      });--%>
<%--    });--%>
<%--  });--%>
</script>
</body>
</html>