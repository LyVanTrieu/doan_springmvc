<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>

<title>Trang chủ</title>

<body>

	<c:forEach var="item" items="${slides}" varStatus="index">
		<div class="site-blocks-cover"
			style="
          background-image: url(<c:url value="assets/user/images/${item.img}"/>);
        "
			data-aos="fade">
			<div class="container">
				<div
					class="
              row
              align-items-start align-items-md-center
              justify-content-end
            ">
					<div class="col-md-4 text-center text-md-left pt-5 pt-md-0">
						<h1 class="mb-2">${item.caption}</h1>
						<div class="intro-text text-center text-md-left">
							<p class="mb-4">${item.content}</p>
							<p>
								<a href='<c:url value="/tat-ca-sp"/>' class="btn btn-sm btn-primary">Shop Now</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>

	<div class="site-section site-section-sm site-blocks-1">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4"
					data-aos="fade-up" data-aos-delay="">
					<div class="icon mr-4 align-self-start">
						<span class="icon-truck"></span>
					</div>
					<div class="text">
						<h2 class="text-uppercase">Miễn Phí Giao Hàng</h2>
						
					</div>
				</div>
				<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4"
					data-aos="fade-up" data-aos-delay="100">
					<div class="icon mr-4 align-self-start">
						<span class="icon-refresh2"></span>
					</div>
					<div class="text">
						<h2 class="text-uppercase">Miễn phí đổi trả</h2>
					
					</div>
				</div>
				<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4"
					data-aos="fade-up" data-aos-delay="200">
					<div class="icon mr-4 align-self-start">
						<span class="icon-help"></span>
					</div>
					<div class="text">
						<h2 class="text-uppercase">Tư vấn</h2>
				
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="site-section site-blocks-2">
		<div class="container">
			<div class="row">
				<c:forEach var="item" items="${categories}">
					<div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0"
						data-aos="fade" data-aos-delay="">
						<a class="block-2-item" href="<c:url value="/san-pham/${item.id }"/>">
							<figure class="image">

								<img src="<c:url value="${item.description }"/>" alt=""
									class="img-fluid" data-pagespeed-url-hash="2182569662"
									onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
							</figure>
							<div class="text" >							
								<h3 style="text-align: center; text-decoration:blink; color:silver;">
									${item.name}
								</h3>
							</div>
						</a>
					</div>
				</c:forEach>

			</div>
		</div>
	</div>
	
	
		<div class="site-section block-3 site-blocks-2 bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 site-section-heading text-center pt-4">
					<h2>Sản Phẩm Mới</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="nonloop-block-3 owl-carousel">
						<c:if test="${newProducts.size() > 0 }">
							<div class="item">
								<c:forEach var="item" items="${newProducts }" varStatus="loop">
									<div class="block-4 text-center">
										<figure class="block-4-image">
										<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">
											<img src="<c:url value="${item.img }"/>"
												alt="Image placeholder" class="img-fluid"
												data-pagespeed-url-hash="1414887416"
												onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />		
										</a>
										</figure>
										<div class="block-4-text p-4">
											<h3>
												<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">${item.name }</a>
											</h3>
											<p class="mb-0">${item.title }</p>
											<p class="text-primary font-weight-bold">
												<fmt:formatNumber type="number" groupingUsed="true"
													value="${item.price}" />
												₫
											</p>
										</div>
									</div>
									<c:if
										test="${ (loop.index + 1) % 1 == 0 || (loop.index + 1) == products.size() }">
									</div>
									<c:if test="${ (loop.index + 1) < products.size()}">
										<div class="item">
									</c:if>
								</c:if>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="site-section block-3 site-blocks-2 bg-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 site-section-heading text-center pt-4">
					<h2>Sản Phẩm Nổi Bật</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="nonloop-block-3 owl-carousel">

						<c:if test="${products.size() > 0 }">
							<div class="item">

								<c:forEach var="item" items="${products }" varStatus="loop">
									<div class="block-4 text-center">
										<figure class="block-4-image">
											<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>" >
													<img src="<c:url value="${item.img }"/>"
												alt="Image placeholder" class="img-fluid"
												data-pagespeed-url-hash="1414887416"
												onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
											</a>
										
										</figure>
										<div class="block-4-text p-4">
											<h3>
												<a href="<c:url value="/chi-tiet-san-pham/${ item.id_product }"/>">${item.name }</a>
											</h3>
											<p class="mb-0">${item.title }</p>
											<p class="text-primary font-weight-bold">
												<fmt:formatNumber type="number" groupingUsed="true"
													value="${item.price}" />
												₫
											</p>
										</div>
									</div>

									<c:if
										test="${ (loop.index + 1) % 3 == 0 || (loop.index + 1) == products.size() }">
							</div>
							<c:if test="${ (loop.index + 1) < products.size()}">
								<div class="item">
							</c:if>
						</c:if>
						</c:forEach>
						</c:if>



					</div>
				</div>
			</div>
		</div>
	</div>


</body>

