<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
    
<div class="content">
       <div class="animated fadeIn">
           <div class="row">
               <div class="col-lg-12">
                   <div class="card">
                       <div class="card-header">
                           <strong class="card-title">Đơn hàng</strong>
                       </div>
                       <div class="card-body">
                            <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp; Tạo đơn</button>
                       </div>
                       <div class="table-stats order-table ov-h">
                           <table class="table ">
                               <thead>
                                   <tr>
                                       <th class="serial">#</th>
                                       <th>Email</th>
                                       <th>Điện thoại</th>
                                       <th>Tên</th>
                                       <th>Địa chỉ</th>
                                       <th>Ghi chú</th>
                                       <th>Tác vụ</th>
                                   </tr>
                               </thead>
                               <tbody>
                               <c:forEach var="item" items="${allBills}" varStatus="loop">
                                   	<tr>
                                           <td><c:out value="${loop.index + 1}"></c:out></td>
                                           <td><c:out value="${item.email}"></c:out></td>
                                           <td><c:out value="${item.phone}"></c:out></td>
                                           <td><c:out value="${item.display_name}"></c:out></td>
                                       	<td><c:out value="${item.address}"></c:out></td>
                                       	<td><c:out value="${item.note}"></c:out></td>                              
                                       <td>
                                       		<span class="card-body">
			                                    <button type="button" class="btn btn-outline-primary btn-sm"><a href="<c:url value="admin-chi-tiet-don-hang"/> "></a>Chi tiết đơn hàng</button>
			                                    <button type="button" class="btn btn-outline-primary btn-sm">Xóa</button>
			                                </span>
                                       </td>
                                       </tr>
                                  	</c:forEach>
                               </tbody>
                           </table>
                       </div> <!-- /.table-stats -->
                    </div>
                </div>

	    </div>
	</div><!-- .animated -->
</div>