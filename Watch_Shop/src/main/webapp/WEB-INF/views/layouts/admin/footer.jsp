<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/admin/taglib.jsp"%>

<footer class="site-footer">
	<div class="footer-inner bg-white">
		<div class="row">
			<div class="col-sm-6">Copyright &copy; 2018 Ela Admin</div>
			<div class="col-sm-6 text-right">
				Designed by <a href="https://colorlib.com">Colorlib</a>
			</div>
		</div>
	</div>
</footer>