<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<footer class="site-footer border-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 mb-5 mb-lg-0">
				<div class="row">
					<div class="col-md-12">
						<h3 class="footer-heading mb-4">Navigations</h3>
					</div>
					<div class="col-md-6 col-lg-4">
						<ul class="list-unstyled">
							<li><a href="#">Sản phẩm trực tuyến</a></li>
							<li><a href="#">Sản phẩm đặc trưng</a></li>
							<li><a href="#">Giỏ hàng</a></li>
							
						</ul>
					</div>
					<div class="col-md-6 col-lg-4">
						<ul class="list-unstyled">
							
							<li><a href="#">Vận chuyển</a></li>
							<li><a href="#">Sản phẩm xu hướng</a></li>
						</ul>
					</div>
					<div class="col-md-6 col-lg-4">
						<ul class="list-unstyled">
							<li><a href="#">Điểm bán hàng</a></li>
							<li><a href="#">Thông tin thêm</a></li>
							<li><a href="#">Nhà phá triển website</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
				<h3 class="footer-heading mb-4">MOVADO 42 mm Automatic kính sapphire</h3>
				<a href="#" class="block-6"> <img
					src="<c:url value="https://cdn.tgdd.vn/Products/Images/7264/246017/movado-0607008-nam-300x300.jpg"/>"
					alt="Image placeholder" class="img-fluid rounded mb-4"
					data-pagespeed-url-hash="25763458"
					onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
					<h3 class="font-weight-light mb-0">Mua đồng hồ đi nhìn làm chi !!!</h3>
					
				</a>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="block-5 mb-5">
					<h3 class="footer-heading mb-4">Liên hệ</h3>
					<ul class="list-unstyled">
						<li class="address">Hà Huy Giáp, khu phố 2, Thạnh Xuân, Quận 12</li>
						<li class="phone"><a href="tel://23923929210">0926122458</a></li>
						<li class="email"><a href="/cdn-cgi/l/email-protection"
							class="__cf_email__"
							data-cfemail="77121a161e1b161313051204043713181a161e195914181a">lyvantrieu309@gmail</a>
						</li>
					</ul>
				</div>
				<div class="block-7">
					<form action="#" method="post">
						<label for="email_subscribe" class="footer-heading">Subscribe</label>
						<div class="form-group">
							<input type="text" class="form-control py-4" id="email_subscribe"
								placeholder="Email" /> <input type="submit"
								class="btn btn-sm btn-primary" value="Send" />
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row pt-5 mt-5 text-center">
			<div class="col-md-12">
				<p>
					Copyright &copy;
					<script data-cfasync="false"
						src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
					<script data-cfasync="false"
						src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
					<script>
						document.write(new Date().getFullYear());
					</script>
					All rights reserved | This template is made with <i
						class="icon-heart" aria-hidden="true"></i> by <a
						href="https://colorlib.com" target="_blank" class="text-primary">Trieu and Nghi</a>
				</p>
			</div>
		</div>
	</div>
</footer>