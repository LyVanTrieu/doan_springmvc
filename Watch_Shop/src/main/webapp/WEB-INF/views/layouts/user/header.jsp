<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>

<header class="site-navbar" role="banner">
	<div class="site-navbar-top">
		<div class="container">
			<div class="row align-items-center">
				<div
					class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
					<form action="" class="site-block-top-search">
						<span class="icon icon-search2"></span> <input type="text"
							class="form-control border-0" placeholder="	Tìm kiếm" />
					</form>
				</div>
				<div
					class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
					<div class="site-logo">
						<a href='<c:url value="/"/>' class="js-logo-clone">Shop Watch</a>
					</div>
				</div>
				<div class="col-6 col-md-4 order-3 order-md-3 text-right">
					<div class="site-top-icons">
						<ul>
							<c:choose>
                                   <c:when test="${not empty loginInfo}">
               							<li><a  href="<c:url value="/dang-nhap"/>"><span class="icon icon-person"></span>Xin chào: ${loginInfo.lastName }</a></li>
                                   </c:when>
                                   <c:when test="${empty loginInfo}">
                                       <li><a  href="<c:url value="/dang-nhap"/>"><span class="icon icon-person"></span></a></li>
                                   </c:when>
                             </c:choose>
							
							<li><a href='<c:url value="/gio-hang"></c:url>' class="site-cart"> 
									<span class="icon icon-shopping_cart"></span>
									<c:if test="${TotalQuantyCart == 0 }">
										<span class="count">0</span>
									</c:if>
									<c:if test="${TotalQuantyCart > 0 }">
										<span class="count">${TotalQuantyCart }</span>
									</c:if>  
									
									
							</a></li>
							<li>
								<a>
								<span class="badge bage-warning">
									<fmt:formatNumber type="number" groupingUsed="true"
													value="${TotalPriceCart}" />
												₫
									</span>
								</a>
							</li>
							<li class="d-inline-block d-md-none ml-md-0"><a href="#"
								class="site-menu-toggle js-menu-toggle"><span
									class="icon-menu"></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="site-navigation text-right text-md-center"
		role="navigation">

		  <div class="container">
            <ul class="site-menu js-clone-nav d-none d-md-block">
              <li class="active">
                <a href='<c:url value="/"></c:url>'>Trang chủ</a>
                
              </li>
              <li>
                <a href="about.html">Bài Viết</a>
              </li>
              <li class="has-children">
                <a href='<c:url value="/tat-ca-sp"/>'>Sản phẩm</a>
                <ul class="dropdown">
                  <c:forEach var="item" items="${categories}">
                  	  <li><a href="<c:url value="/san-pham/${item.id }"/>">${item.name}</a></li>
                  </c:forEach>
                
             
                </ul>
              </li>
              <li><a href='<c:url value="/gio-hang"></c:url>'>Giỏ hàng</a></li>
              <li><a href="contact.html">Liên hệ</a></li>
            </ul>
          </div>

	</nav>
</header>