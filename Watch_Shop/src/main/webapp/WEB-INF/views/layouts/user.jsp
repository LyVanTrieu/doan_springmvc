<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
		<title><decorator:title default="Master-Layout" /></title>
		<meta charset="utf-8" />
		<meta name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		
		<link rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Mukta:300,400,700" />
		
		<link rel="stylesheet" href="<c:url value="/assets/user/style.css"/>" />
		
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
	
		<link rel="shortcut icon" href="<c:url value="assets/user/watch1.png"/>">

<decorator:head />
</head>

<body>
	<div class="site-wrap">

		<!-- header -->
		<%@include file="/WEB-INF/views/layouts/user/header.jsp"%>
		<!-- header -->

		<!-- body -->
		<decorator:body />
		<!-- body -->

		<!-- footer -->
		<%@include file="/WEB-INF/views/layouts/user/footer.jsp"%>
		<!-- footer -->

	</div>

	<script src="<c:url value="assets/user/js/jquery-3.3.1.min.js"/>"></script>
	<script
		src="<c:url value="assets/user/js/jquery-ui.js+popper.min.js.pagespeed.jc.wCOQF-sYqe.js"/>"></script>
	<script>
		eval(mod_pagespeed_sHE7AAz7N7);
	</script>
	<script>
		eval(mod_pagespeed_uuY4o1g_Jj);
	</script>
	<script src="<c:url value="assets/user/js/bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="assets/user/js/owl.carousel.min.js+jquery.magnific-popup.min.js+aos.js+main.js.pagespeed.jc.XC9hmUTyoV.js"/>"></script>
	<script>
		eval(mod_pagespeed_kniwPP9i1Y);
	</script>
	<script>
		eval(mod_pagespeed_QnDzTpeb4i);
	</script>
	<script>
		eval(mod_pagespeed_qM2qpyS09V);
	</script>
	<script>
		eval(mod_pagespeed_nGytBXrOXJ);
	</script>
	<script async
		src='<c:url value="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></c:url>'></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag("js", new Date());

		gtag("config", "UA-23581568-13");
	</script>
	<script defer src='<c:url value="https://static.cloudflareinsights.com/beacon.min.js"></c:url>'
		data-cf-beacon='{"rayId":"6852b5362c664a89","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.8.1","si":10}'></script>


	<decorator:getProperty property="page.script"></decorator:getProperty>
	
</body>

</html>
